(function () {
    'use strict';

    var app = angular.module('calcApp');
    app.factory('calcCalculate', function() {
        var tmpCheckResult = false;
        var tmpCheckOperation = false;
      
        return {
        calculate: function(result) {
            tmpCheckResult = true;
            if (result.length) {
                return result;
            }
        },
        clear: function(result) {
            return result = '';
        },
        setNewValue: function(result,values,currentValue) {
            if (tmpCheckResult) {
                result = '';
                tmpCheckResult = false;
            }
            if ((result.length === 0) && (values.indexOf(currentValue) === -1)){
                return result;
            }
            else {
                return result += currentValue;
            }
        }
    }
    });

})();