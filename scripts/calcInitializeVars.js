(function () {
    'use strict';

    var app = angular.module('calcApp');
    app.factory('calcInitVar', function() {
       var initVars = { result: '',
                        resultButton:'=',
                        values: ['1','2','3','4','5','6','7','8','9','0','.','C'],operations: ['+','-','*','/'],
                        tmpCheckResult: false,
                        tmpCheckOperation: false
                        };
       return initVars;
    });
})();