(function () {
    'use strict';

    var app = angular.module('calcApp',[]);
    app.controller('CalcController',['$scope', 'calcInitVar', 'calcCalculate',  function CalcController($scope, calcInitVar, calcCalculate) {
        $scope.result = calcInitVar.result;
        $scope.resultButton = calcInitVar.resultButton;
        $scope.values = calcInitVar.values;
        $scope.operations = calcInitVar.operations;
        var tmpCheckOperation = calcInitVar.tmpCheckOperation;

        $scope.click = function(currentValue) {
            switch(currentValue) {
                case '=':
                    $scope.result = $scope.$eval(calcCalculate.calculate($scope.result));
                    tmpCheckOperation = false;
                    break;
                case '+':
                case '-':
                case '*':
                case '/':
                    if (!tmpCheckOperation) $scope.result = calcCalculate.setNewValue($scope.result,$scope.values,currentValue);
                    tmpCheckOperation = true;
                    break;
                case 'C':
                    $scope.result = calcCalculate.clear();
                    tmpCheckOperation = false;
                    break;
                default:
                    $scope.result = calcCalculate.setNewValue($scope.result,$scope.values,currentValue);
                    tmpCheckOperation = false;
            }
        };
    }]);
})();