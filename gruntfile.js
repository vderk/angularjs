module.exports = function(grunt) {

grunt.initConfig({
    less: {
    	dev: {
    		options: {
    			path: 'styles/'
    		},
    		files: {
    			'dist/styles.css' : 'styles/styles.less'
    		}
    	}
    },
    uglify: {
    	dev: {
    		files: {
    			'dist/scripts.min.js' : ['scripts/calcController.js','scripts/calcInitializeVars.js','scripts/calcCalculate.js','scripts/app.js']
    		}
    	}
    },
	connect: {
		dev: {
			port: 9000
		}
	}
});

    
grunt.loadNpmTasks('grunt-contrib-less');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-connect');

grunt.registerTask('build', ['less:dev', 'uglify:dev', 'connect:dev']);

};